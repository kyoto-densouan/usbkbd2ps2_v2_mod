# README #

これは「たま吉さん」が作成された、「USBKBD2PS2_v2 USBキーボード PS/2キーボード変換 for IchigoJam v2」を改造したものです。 
https://github.com/Tamakichi/Arduino_USBToPS2 

一般的な使用上のメリットがないため、ここでひっそりと公開します。 
USBキーボード-PS/２キーボード変換を利用したい方は、上記オリジナルを使用してください。 

##変更内容##

A6ピンの状態により、変換テーブルを切り替えられるようにしました。 
切り替えた変換テーブルでは、カーソルキーをテンキーに割り当てるようにしたことで、 
テンキーがないキーボードでもテンキー操作同等の操作ができるようになります。 


